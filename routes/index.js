const express = require('express');
const Axios = require("axios");
const {payment, notificationHandler, createInvoice, applyDiskon} = require("../controller/payment");
const {dataLayanan, dataLayananTambahan, diskon} = require("../controller/globalData");
const router = express.Router();

router.get('/', (req, res, next) => {
    res.send('Be duitku service!')
})
router.post('/createInvoice', createInvoice)
router.post('/payment', payment)
router.post('/notificationHandler', notificationHandler)

router.get('/dataLayanan', dataLayanan)
router.get('/dataLayananTambahan', dataLayananTambahan)
router.get('/diskon', diskon)


router.post('/applyDiskon', applyDiskon)
module.exports = router;
