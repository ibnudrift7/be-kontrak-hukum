// noinspection JSVoidFunctionReturnValueUsed

const db = require('./connection')
require("dotenv").config({
    path: ".env"
})
const crypto = require('crypto-js');

const axios = require("axios");


/**
 *
 * @param number {number}
 * @return {string}
 */
exports.numberFormat = number => new Intl.NumberFormat("id-ID", {style: "currency", currency: "IDR"}).format(number);


/**
 * Global check exist
 *
 * @param query {Knex.QueryBuilder<> | Knex.QueryInterface<>}
 * @return {Promise<boolean> | Promise<error>}
 */
exports.checkExistTable = async query => {
    const [[{check}]] = await db.raw(`select exists(${query.first(1).toQuery()}) as \`check\``)
    return !!check
}


/**
 * Global check exist
 *
 * @return {Promise<boolean> | Promise<error>}
 * @param data
 */
exports.paymentDuitku = async (data) => {
    try {
        let timestamp = new Date().getTime();
        return await axios({
            method: 'POST',
            url: process.env.DUITKU_SERVICE_URL,
            data: data,
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json; charset=UTF-8",
                "x-duitku-signature": crypto.SHA256(`${process.env.DUITKU_MERCHANT_ID}${timestamp}${process.env.DUITKU_API_KEY}`).toString(),
                "x-duitku-timestamp": `${timestamp}`,
                "x-duitku-merchantcode": `${process.env.DUITKU_MERCHANT_ID}`,
            }
        })
            .then(response => {
                return response.data
            })
            .catch(err => {
                console.log(err)
                let errorJson = {
                    error: err.code,
                    status: err.response.status,
                    statusMessage: err.response.statusText,
                    reason: err.response.data,
                }
                ({}, errorJson)
            })


    } catch (e) {
        console.log(e)
    }
}

