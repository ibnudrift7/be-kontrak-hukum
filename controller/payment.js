const db = require('../connection')
const {paymentDuitku} = require("../globalHelper");
const crypto = require("crypto");
const randomstring = require("uuidv4");
const moment = require("moment");


/**
 * Payment controller Duitku
 *
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 */
exports.payment = async (req, res, next) => {
    try {
        const data = req.body

        res.status(200).json(data)
    }catch (e) {
        console.log(e)
    }
}

/**
 * Payment controller Duitku
 *
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 */
exports.applyDiskon = async (req, res, next) => {
    try {
        const {diskon_name} = req.body

        const data = await db('diskon').where({diskon_name: diskon_name}).select('diskon_value', 'diskon_type', 'diskon_name').first()
        if(!data) {
                res.status(400).json({message: 'Diskon tidak ditemukan'})
        }else{
                res.status(200).json({message: 'Diskon bisa di apply', data:data })
        }
    }catch (e) {
        console.log(e)
    }
}

/**
 * Payment controller Duitku
 *
 * @param req {e.Request}
 * @param res {e.Response}
 * @param next {e.NextFunction}
 */
exports.createInvoice = async (req, res, next) => {
    try {
        const {dataPT} = req.body

        const [id_user] = await db('user').insert({
            nama_lengkap: dataPT.nama_lengkap,
            no_telp: dataPT.no_tel,
            email: dataPT.email,
            nama_badan_usaha1: dataPT.nama_usaha1,
            nama_badan_usaha2: dataPT.nama_usaha2 ? dataPT.nama_usaha2 : null,
            nama_badan_usaha3: dataPT.nama_usaha3 ? dataPT.nama_usaha3 : null,
        }, ['id'])


        for (const data of dataPT.dataPemegangSaham) {
            await db('user_pemegang_saham').insert({
                id_user,
                jabatan: data.jabatan,
                ktp: data.ktp_name,
                npwp: data.npwp_name
            })

            if (data.ktp_file){
                const ktp_file = data.ktp_file.replace(/^data:image\/jpg;base64\/png;base64\/jpeg;base64,/, "");
                require("fs").writeFile(`${__dirname}/../upload/ktp/${data.ktp_name}`, ktp_file, 'base64', function(err) {
                    console.log(err);
                });
            }

            if (data.npwp_file){
                const npwp_file = data.npwp_file.replace(/^data:image\/jpg;base64\/png;base64\/jpeg;base64,/, "");
                require("fs").writeFile(`${__dirname}/../upload/npwp/${data.npwp_name}`, npwp_file, 'base64', function(err) {
                    console.log(err);
                });

            }
        }

        const dataDiskon = await db('diskon').where({diskon_name: dataPT.diskonName}).select('id').first()

        const [order_id] = await db('order').insert({
            id_user: id_user,
            invoice: 'hukum-online-'+moment().format('YYYYMMDDHHmmss'),
            amount: dataPT.total,
            layanan: dataPT.dataPembayaran[0].nama_layanan,
            layanan_tambahan: dataPT.dataPembayaran.length > 1 ? 1 : 0,
            no_reference: '',
            id_diskon: dataDiskon.id
        }, ['id'])


        if (dataPT.dataPembayaran.length > 1) {
            for (let i = 0; i < dataPT.dataPembayaran.length; i++) {
                if (i > 0) {
                    await db('order_tambahan_layanan').insert({
                        id_order: order_id,
                        nama_layanan_tambahan: dataPT.dataPembayaran[i].nama_layanan
                    })
                }
            }
        }

        const {invoice} = await db('order').where({id: order_id}).select('invoice').first('invoice')

        if (dataPT.diskonValue == 100) {
            return res.status(200).json({message: 'thankyou'})
        }
        const reqData = {
            paymentAmount: dataPT.total,
            merchantOrderId: invoice,
            productDetails: dataPT.dataPembayaran[0].nama_layanan,
            email: dataPT.email,
            additionalParam: '',
            merchantUserInfo: '',
            customerVaName: dataPT.nama_lengkap,
            phoneNumber: dataPT.no_tel,
            itemDetails: dataPT.dataPembayaran.map(data => {
                return {
                    name: data.nama_layanan,
                    quantity: 1,
                    price: data.price
                }
            }),
            customerDetail: {
                firstName: dataPT.nama_lengkap,
                lastName: '',
                email: dataPT.email,
                phoneNumber: dataPT.no_tel,
                billingAddress: {
                    firstName: dataPT.nama_lengkap,
                    lastName: '',
                    address: '',
                    city: '',
                    postalCode: '',
                    phone: dataPT.no_tel,
                    countryCode: 'ID'
                },
                shippingAddress: {
                    firstName: dataPT.nama_lengkap,
                    lastName: '',
                    address: '',
                    city: '',
                    postalCode: '',
                    phone: dataPT.no_tel,
                    countryCode: 'ID'
                }
            },
            returnUrl: 'http://localhost:3000/thankyou',
            callbackUrl: 'https://c3bd-140-0-13-180.ngrok.io/notificationHandler',
            expiryPeriod: 1440
        }



        const data = await paymentDuitku(reqData)

        await db('order').where({id: order_id}).update({
            no_reference: data.reference
        })

        res.status(200).json(data)
    } catch (e) {
        console.log(e)
        res.status(400).json(e)
    }
}

/**
 *
 * @param req {e.Request}
 * @param res {e.Response}
 */
exports.notificationHandler = async (req, res, next) => {
    try {
        const {
            merchantCode,
            amount,
            merchantOrderId,
            productDetail,
            additionalParam,
            paymentCode,
            resultCode,
            merchantUserId,
            reference,
            signature
        } = req.body

        console.log(req.body)
        //
        // console.log(req)
        console.log(merchantCode, amount, merchantOrderId, productDetail, additionalParam, paymentCode, resultCode, merchantUserId, reference, signature)

        if (resultCode === '00') {
            await db('order').where({no_reference: reference}).update(
                {
                    id_status_order: 2,
                }
            )
        }
        else if (resultCode == '01'){
            await db('order').where({no_reference: reference}).update(
                {
                    id_status_order: 3,
                }
            )
        }


        res.status(200).json()
    }catch (e) {
        console.log(e)
        next(errorHandlerSyntax(AXIOS_ERROR, e));
    }

}

// exports.callback = (request) => {
//
//     if (!request) {
//         throw new Exception('Access denied');
//     }
//
//     if(typeof request == "string") {
//         request = JSON.parse(request)
//     }
//
//     if(!request.merchantCode || !request.amount || !request.merchantOrderId || request.signature){
//         throw new Error("Bad Parameter")
//     }
//
//     if(req.signature != crypto.MD5(`${process.env.DUITKU_MERCHANT_ID}${callback.amount}${callback.merchantOrderId}${process.env.DUITKU_API_KEY}`).toString()){
//         throw new Error("Wrong Signature")
//     }
//     else{
//         console.log("mantappppp")
//     }
//
//     console.log(request)
//     return request;
// }