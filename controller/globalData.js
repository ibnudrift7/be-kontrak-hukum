const db = require('../connection')

exports.dataLayanan = async (req, res) => {
    try {
        const data = await db('master_layanan')
        res.status(200).json({message: "data layanan", data})
    } catch (error) {
        res.status(500).json(error)
    }
}

exports.dataLayananTambahan = async (req, res) => {
    try {
        const data = await db('master_layanan_tambahan')
        res.status(200).json({message: "data layanan tambahan", data})
    } catch (error) {
        res.status(500).json(error)
    }
}

exports.diskon = async (req, res) => {
    try {
        const data = await db('diskon')
        res.status(200).json({message: "diskon data", data})
    } catch (error) {
        res.status(500).json(error)
    }
}