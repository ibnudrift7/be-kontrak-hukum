require("dotenv").config({
    path: ".env"
})

const duitkuConfig = {
    merchantCode : process.env.DUITKU_MERCHANT_ID,
    apiKey : process.env.DUITKU_API_KEY,
    passport : process.env.NODE_ENV === "production" ? true : false,
    callbackUrl : "https://example/route/callback",
    returnUrl : "https://example/route/return",
    expiryPeriod: 1440
};

module.exports = duitkuConfig;